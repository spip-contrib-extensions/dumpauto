<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
	
/**
 * Nettoyage journalier des sauvegardes SQLite obsolètes.
 *
 * @param timestamp $last
 *
 * @return int
 */
function genie_dumpauto_nettoyer($last) {
	// Lancement du nettoyage journalier des sauvegardes
	include_spip('dumpauto_fonctions');
	dumpauto_nettoyer(['auteur' => 'cron']);
	return 1;
}
