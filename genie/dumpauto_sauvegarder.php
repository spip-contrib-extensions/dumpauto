<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Génération d'une sauvegarde périodique complète par le cron.
 *
 * @param  $last
 *
 * @return int
 */
function genie_dumpauto_sauvegarder($last) {
	// Lancement de la sauvegarde périodique SPIP sqlite
	include_spip('dumpauto_fonctions');
	dumpauto_sauvegarder(['auteur' => 'cron']);
	return 1;
}

