
<?php

/**
 * Fonctions utiles au plugin dumpauto
 *
 * @plugin     dumpauto
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\dumpauto\Fonctions
 *
 * Autorisation, token, filtres et API dumpauto
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/dump');
include_spip('base/dump');
include_spip('inc/config');

/**
 * Fonction de sauvegarde appelée par le cron SPIP
 *
 */
function dumpauto_sauvegarder ($options = []) {
	spip_log('dumpauto:initialisation','dumpauto.'._LOG_DEBUG); // Bug spip_log ? 1ere ligne non écrite... A voir
	
	// Nom du fichier de suivi
	$status_file= 'status_dump_cron';
	$status_file = _DIR_TMP . $status_file;

	// Nom du fichier de sauvegarde
	$dir_dump = dump_repertoire();
	$nom_sauvegarde = basename(dump_nom_fichier($dir_dump, 'sqlite'), '.sqlite');
	$archive = $dir_dump.$nom_sauvegarde.'.sqlite';
	spip_log(_T('dumpauto:lancement_sauvegarde').' '.joli_repertoire($archive),'dumpauto.'._LOG_INFO_IMPORTANTE);
	
	// Identification de l'auteur ayant déclenché la sauvegarde
	$auteur = $options['auteur'] ?: $GLOBALS['visiteur_session']['id_auteur'];
	if ($auteur == 'cron') {
		$auteur = 'SPIP';
	}

	// Sauvegarde de toutes les tables sauf celles exclues par defaut
	$exclude = lister_tables_noexport();
	[$tables, ] = base_liste_table_for_dump($exclude);
	$tables = base_lister_toutes_tables('', $tables, $exclude);

	// Initialisation du dump
	$res = dump_init($status_file, $archive, $tables);
	utiliser_langue_visiteur();

	if ($res !== true) {
		spip_log(T("dumpauto:erreur_initialisation").': '.$res,'dumpauto.'._LOG_ERREUR);
	} else {
		// Lancement de la sauvegarde
		if (dumpauto_copier_base($status_file)) {
			// Fin de la sauvegarde
			dump_end($status_file, 'sauvegarder');
			
			// Afficher les tables sauvegardées
			$detail = dumpauto_afficher_tables_sauvegardees($status_file);
			spip_log(_T("dumpauto:details_sauvegarde").$detail,'dumpauto.'._LOG_INFO_IMPORTANTE);
			
			// Un pipeline post_sauvegarde pour que d'autres plugins puissent agir à ce moment là.
			pipeline(
				'post_sauvegarde',
				[
				'args' => [
					'auteur'  => $auteur,
					'type'    => 'dumpauto_sauver',
					'archive' => $archive ?? ''
					],
				'data' => ''
				]);
				
				// Notification si demandé dans la configuration
				if(lire_config('dumpauto/notification')){
			
					// Construction du sujet du mail
					include_spip('inc/texte');
					$base = $GLOBALS['connexions'][0]['db'];
					$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
							. "][Dumpauto] "
							. _T('dumpauto:message_sauvegarder_sujet');
					
					// Construction du message
					$message = _T('dumpauto:info_message_sauvegarder').$detail;
		
					// Détermination de la liste des destinataires
					$mails = lire_config('dumpauto/mails');
					$tous = ($mails) ? explode(',', $mails) : array();
					// Ajout du webmaster
					$tous[] = $GLOBALS['meta']['email_webmaster'];
			
					// Pasage par le pipeline "notifications_destinataires"
					$destinataires = pipeline('notifications_destinataires',
						array(
							'args'=>array('quoi'=>'dumpauto','id'=>'','options'=>''),
							'data'=>$tous)
							);
							
					// Envoi des emails
					$envoyer_mail = charger_fonction('envoyer_mail','inc');
					foreach($destinataires as $email){
						$envoyer_mail($email, $sujet, $message, '', '');
					}
				}
			
			
			} else {
			spip_log (_T("dumpauto:erreur_copie_base").': '.$res,'dumpauto.'._LOG_ERREUR);
			}



	// Fin de sauvegarde automatique
	spip_log(_T("dumpauto:info_sauvegarde_terminee").' '.joli_repertoire($archive). " (".taille_en_octets(filesize($archive)).")",'dumpauto.'._LOG_INFO_IMPORTANTE);

	}
} 

/**
 * Fonction principale de sauvegarde sqlite
 * Copie de base a base (dans l'API de SPIP)
 *
 * @param string $status_file Nom du fichier de status (stocke dans _DIR_TMP)
 * @return bool
 */
function dumpauto_copier_base($status_file) {

	$status = [];
	$status_file = _DIR_TMP . basename($status_file,'.txt') . '.txt';

	// Lecture fichier de suivi
	if (!lire_fichier($status_file, $status)
		or !$status = unserialize($status)){
		spip_log(T("dumpauto:erreur_lecture_fichier").':  '.$status_file,'dumpauto.'._LOG_ERREUR);
		} 
	else {
		// Détermination de la durée maximum d'une itération de sauvegarde pour éviter les timeout sqlite
		$timeout = ini_get('max_execution_time');
		// valeur conservatrice si on a pas reussi a lire le max_execution_time
		if (!$timeout) {
			$timeout = 30;
		} // parions sur une valeur tellement courante ...
		$max_time = time() + $timeout / 2;
		}
		
		// initialisation du dump		
		dump_serveur($status['connect']);
		spip_connect('dump');
			
		// Copie de base à base (dans l'API de SPIP)	
		$options = [
				'callback_progression' => '',
				'max_time' => $max_time,
				'no_erase_dest' => lister_tables_noerase(),
				'where' => $status['where'] ?: [],
			];
		
		$res = false;

		while (!$res) {
				$res = base_copier_tables($status_file, $status['tables'], '', 'dump', $options);
				spip_log(_T("dumpauto:sauvegarde_en_cours") . ' (' . (is_countable($status['tables']) ? count($status['tables']) : 0) . ') ','dumpauto.'._LOG_INFO_IMPORTANTE);			
		}
		
		return $res;
}

/**
 * Afficher les erreurs survenues dans la sauvegarde
 *
 * Reprise de la fonction dump_afficher_tables_sauvegardees de plugins-dist et adapté pour le cron
 *
 * @param string $status_file Nom du fichier qui contient le statut de la sauvegarde sous une forme serialisee
 * @return string               Code HTML a afficher
 */
function dumpauto_afficher_tables_sauvegardees($status_file) {
	$status = dump_lire_status($status_file);
	$tables = $status['tables_copiees'];

	// lister les tables sauvegardees et aller verifier dans le dump
	// qu'on a le bon nombre de donnees
	dump_serveur($status['connect']);
	spip_connect('dump');

	foreach ($tables as $t => $n) {
		$n = abs(intval($n));
		$n_dump = intval(sql_countsel($t, '', '', '', 'dump'));
		$res = "$t ";
		if ($n_dump == 0 and $n == 0) {
			$res .= '(' . _T('dumpauto:aucune_donnee') . ')';
		} else {
			$res .= "($n_dump/$n)";
		}
		if ($n !== $n_dump) {
			$res = $res.' '. _T('dumpauto:ecart_nb_enregistrement');
		}
		$tables[$t] = $res;
	}
	
	$corps = "\n - ".join("\n - ",$tables);
		
	return $corps;
}

/**
 * Fonction de nettoyage appelée par le cron SPIP
 *
 */
function dumpauto_nettoyer ($options = []) {
	spip_log('dumpauto:initialisation','dumpauto.'._LOG_DEBUG); // Bug spip_log ? 1ere ligne non écrite... A voir

	// Initialiser la liste des archives supprimées fournie en retour
	$liste = [];
	
	// Durée de conservation par défaut de 15 jours
	$duree = (lire_config('dumpauto/duree', 15));
	
	// Nombre minimum de sauvegardes à conserver
	$nbr_garder = intval(lire_config('dumpauto/nbr_garder'));

	
	$limite = date_add(date_create('now'),date_interval_create_from_date_string("-".$duree." days"));
	spip_log(_T('dumpauto:lancement_nettoyage').' '.date_format($limite,"d-m-Y"),'dumpauto.'._LOG_INFO_IMPORTANTE);

	$duree_sauvegarde = intval( $duree);
	if ($duree_sauvegarde > 0) {
		// Stocker la date actuelle pour calculer l'obsolescence
		$temps = time();

		// Scan des sauvegardes sqlite existantes
		$dir_dump = dump_repertoire();
		include_spip('inc/flock');
		$sauvegardes = preg_files($dir_dump, '\.' . 'sqlite' . '$', 100, false);
        $nbr_sauvegardes = count($sauvegardes);

		foreach ($sauvegardes as $_sauvegarde) {
			// Nombre minimum de sauvegardes
			$nbr_sauvegardes -= 1;
			if ($nbr_garder AND $nbr_sauvegardes <= $nbr_garder) {
                break;
            }
			$date_fichier = filemtime($_sauvegarde);
			// Suppression des sauvegardes obsolètes
			if ($temps > ($date_fichier + $duree_sauvegarde * 3600 * 24)) {
				supprimer_fichier($_sauvegarde);
				$liste[] = $_sauvegarde;
			}
		}
		// détail des sauvegardes supprimées
		if ($liste) {
			$detail = "\n - ".join("\n - ",$liste);
			spip_log(_T("dumpauto:details_nettoyage").$detail,'dumpauto.'._LOG_INFO_IMPORTANTE);
			
			// Notification si demandé dans la configuration
			if(lire_config('dumpauto/notification')){
			
					// Construction du sujet du mail
					include_spip('inc/texte');
					$base = $GLOBALS['connexions'][0]['db'];
					$sujet = "[" . typo($GLOBALS['meta']['nom_site'])
							. "][Dumpauto] "
							. _T('dumpauto:message_nettoyer_sujet');
					
				// Construction du message
					$message = _T('dumpauto:info_message_nettoyer').$detail;
		
					// Détermination de la liste des destinataires
					$mails = lire_config('dumpauto/mails');
					$tous = ($mails) ? explode(',', $mails) : array();
					// Ajout du webmaster
					$tous[] = $GLOBALS['meta']['email_webmaster'];
			
					// Passage par le pipeline "notifications_destinataires"
					$destinataires = pipeline('notifications_destinataires',
								array(
									'args'=>array('quoi'=>'dumpauto','id'=>'','options'=>''),
									'data'=>$tous)
								);
							
					// Envoi des emails
					$envoyer_mail = charger_fonction('envoyer_mail','inc');
					foreach($destinataires as $email){
						$envoyer_mail($email, $sujet, $message, '', '');
					}
			}
		}
	}
		
	
	
	// Fin du nettoyage automatique
	$nombre = count($liste) ?? '0';
	spip_log(_T("dumpauto:info_nettoyage_termine",['nombre' => $nombre]),'dumpauto.'._LOG_INFO_IMPORTANTE);

	return $liste;
}