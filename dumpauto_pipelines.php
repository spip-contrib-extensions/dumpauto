<?php

/**
 * Utilisations de pipelines par dumpauto
 *
 * @plugin     dumpauto
 * @copyright  2023
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\dumpauto\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function dumpauto_taches_generales_cron($taches_generales) {

	include_spip('inc/config');
	if (lire_config('dumpauto/sauvegarde')) {
		// Sauvegardes périodiques dump SQLite
		$jour = lire_config('dumpauto/frequence', 1);
		if(is_numeric($jour) && $jour > 0) {
		$taches_generales['dumpauto_sauvegarder'] = $jour * 24 * 3600;
		}
	}

	if (lire_config('dumpauto/nettoyage')) {
		// Nettoyage journalier des sauvegardes obsolètes
		$duree = lire_config('dumpauto/duree',15);
		if(is_numeric($duree) && $duree > 0) {
		$taches_generales['dumpauto_nettoyer'] = 24 * 3600;
		}
	}
		
return $taches_generales;

}