<?php

/**
 * Définit les autorisations du plugin dumpauto
 *
 * @plugin     Dumpauto
 * @copyright  2024
 * @author     JMarc_64
 * @licence    GNU/GPL
 * @package    SPIP\dumpauto\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser 
 */

function dumpauto_autoriser() {
}

function autoriser_dumpauto_configurer_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('sauvegarder', $type, $id, $qui, $opt);
}
