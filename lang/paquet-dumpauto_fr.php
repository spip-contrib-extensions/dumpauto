<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// C
	'configuration_dumpauto' => 'Dumpauto',
	
	// D
	'dumpauto_description' => 'Sauvegardes automatiques du dump sqlite SPIP',
	'dumpauto_nom' => 'Dumpauto',
	'dumpauto_slogan' => 'Automatisation des sauvegardes SPIP au format dump SQLite',
];
