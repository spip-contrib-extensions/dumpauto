<?php

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = [

	// A
	'aucune_donnee' => 'vide',

	// D
	'dumpauto_titre' => 'dumpauto',
	
	// E
	'explication_sauvegarde' => 'Activer les sauvegardes périodiques',
	'explication_frequence' => 'Saisir la fréquence des sauvegardes en jours',
	'explication_nettoyage' => 'Activer le nettoyage journalier',
	'explication_duree' => 'Saisir la durée de conservation des sauvegardes en jours',
	'explication_nbr_garder' => 'Saisir le nombre minimum de sauvegardes à conserver',
	'explication_notification' => 'Si vous souhaitez être prévenu des sauvegardes et des nettoyages automatiques, activez les notifications',
	'explication_mails' => 'Saisir les adresses emails des destinataires en les séparant par des virgules ",". Ces adresses s\'ajoutent à celle du webmestre du site',
	'erreur_initialisation' => 'Erreur d\'initialisation de la sauvegarde automatique',
	'erreur_copie_base' => 'Erreur dans la copie de base à base',
	'erreur_lecture_fichier' => 'Erreur dans la lecture du fichier',
	'ecart_nb_enregistrement' => '/!\ Ecart nombre d\'enregistrements',

	// C
	'cfg_titre_parametrages' => 'Paramétrages',

	// D
	'detail_sauvegarde' => 'Détail de la sauvegarde automatique:',
	'detail_nettoyage' => 'Détail des sauvegardes supprimées:',

	// I
	'info_sauvegarde_terminee' => 'Sauvegarde automatique terminée',
	'info_nettoyage_termine' => 'Nettoyage automatique terminé: @nombre@ fichier(s) supprimé(s)',
	'info_message_sauvegarder' => 'Table(s) Dump SQLite sauvegardée(s):',
	'info_message_nettoyer' => 'Fichier(s) Dump SQLite supprimé(s):',

	// L
	
	'legende_traitement' => 'Traitements automatiques',
	'legende_notification' => 'Notifications',
	'label_sauvegarde' => 'Sauvegardes périodiques',
	'label_frequence' => 'Fréquence',
	'label_notification' => 'Sauvegarde & Nettoyage',
	'label_nettoyage' => 'Nettoyage journalier',
	'label_duree' => 'Durée',
	'label_nbr_garder' => 'Minimum',
	'label_mails' => 'Adresses email',
	'lancement_sauvegarde' => 'Lancement automatiquede la sauvegarde',
	'lancement_nettoyage' => 'Lancement automatique du nettoyage des sauvegardes antérieures au',

	// M
	'message_sauvegarder_sujet' => 'Sauvegarde Dump SQLite',
	'message_nettoyer_sujet' => 'Nettoyage Dump SQLite',

	// S
	'sauvegarde_en_cours' => 'Sauvegarde automatique en cours',

	// T
	'titre_page_configurer_dumpauto' => 'Configuration Dumpauto',
	'texte_sauvegarde' => 'Activer les sauvegardes automatiques',
	'texte_nettoyage' => 'Activer le nettoyage journalier des sauvegardes',
	'texte_notification' => 'Activer les notifications de sauvegarde et de nettoyage',

];
